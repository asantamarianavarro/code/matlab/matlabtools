clear rosbag_wrapper;
clear ros.Bag;
clear all

% Add paths
% addpath(genpath(strcat(pwd,'/../rosbag_matlab')));
% addpath(genpath(strcat(pwd,'/../plot_pub')));
addpath(genpath(strcat(pwd,'/../rosbag_matlab')));
addpath(genpath(strcat(pwd,'/../plot_pub')));

% % bag name
bagfile='/Users/asantamaria/Desktop/rosbags_subT/stix_army_run2_longfail_2019-04-10-11-50-36.bag';

% Load bagfile
bag = ros.Bag.load(strcat(pwd,bagfile));
%clear bagfile;

% Load Odometry 1
display('Loading ORBSlam VIO data...');
[torbslam,orbslam] = odom_from_rosbag(bagfile,'/rollo/mavros/local_position/odometry');

% Load Odometry 2
display('Loading QSF VIO data...');
[tqsf,qsf] = odom_from_rosbag(bagfile,'/rollo/qsf/local_position/odometry');

% Load Odometry 3
display('Loading LIO data...');
[tlio,lio] = odom_from_rosbag(bagfile,'/rollo/lio/local_position/odometry');

% Load Odometry 4
display('Loading Resiliency data...');
[tres,res] = odom_from_rosbag(bagfile,'/rollo/resiliency/odometry');

%% visualization

% % Plot trajectory
% fig = figure();
% subplot(1,1,1);
% hold on;
% set(fig, 'Name','reconstructed vs Ground Truth');
% plot(traj_noisy(:,1),traj_noisy(:,2),'r');
% plot(traj_noisy(1,1),traj_noisy(1,2),'og'); %start point
% plot(traj_noisy(end,1),traj_noisy(end,2),'xg'); %end point
% plot(right_pose.ts(:), groundTruth(:,2),'b');
% legend('reconstructed','right_pose','end', 'ground_truth');
% title('trajectory plot (X) wrt time')
% grid on
% 
% subplot(1,1,2)
% hold on;
% plot(traj_noisy(:,1),traj_noisy(:,3),'r');
% plot(traj_noisy(1,1),traj_noisy(1,3),'og'); %start point
% plot(traj_noisy(end,1),traj_noisy(end,3),'xg'); %end point
% plot(right_pose.ts(:), groundTruth(:,3),'b');
% legend('reconstructed','start','end', 'ground_truth');
% title('trajectory plot (Y) wrt time')
% grid on
% 
% subplot(1,1,3)
% hold on;
% plot(traj_noisy(:,1),traj_noisy(:,4),'r');
% plot(traj_noisy(1,1),traj_noisy(1,4),'og'); %start point
% plot(traj_noisy(end,1),traj_noisy(end,4),'xg'); %end point
% plot(right_pose.ts(:), groundTruth(:,4),'b');
% legend('reconstructed','start','end', 'ground_truth');
% title('trajectory plot (Z) wrt time')
% grid on


%% lets plot 3 cycles of 50Hz AC voltage
f = 50;
Vm = 10;
phi = pi/4;

% generate the signal
t = [0:0.0001:3/f];
th = 2*pi*f*t;
v1 = Vm*sin(th);
v2 = Vm*sin(th - phi);
v3 = Vm*sin(th - phi*2);
v4 = Vm*sin(th - phi*2.5);


fig1 = figure;
plot(t*1E3, v1);
hold on;
plot(t*1E3, v2);
plot(t*1E3, v3);
plot(t*1E3, v4);
hold off;

%% change properties
plt = Plot();
plt.XLabel = '[s]'; % xlabel
plt.YLabel = '[m/s]'; %ylabel
plt.YTick = [-10, 0, 10]; %[tick1, tick2, .. ]
plt.XLim = [0, 80]; % [min, max]
plt.YLim = [-11, 11]; % [min, max]

plt.Colors = setColors(4,'qualitative');

plt.LineWidth = [2, 2, 2, 1]; % three line widths
plt.LineStyle = {'-', '-', '-', '-'}; % three line styles
plt.Legend = {'VIO1', 'VIO2', 'LIO', 'Resilient out'}; % legends
plt.MarkerSize = [10, 10, 10, 10];
plt.MarkerSpacing = [10, 10, 10, 10];
plt.Markers = {'o', 's', 'd',''};

plt.LegendBox = 'on';

% Save? comment the following line if you do not want to save
%plt.export('plotMultiple2.eps'); 
