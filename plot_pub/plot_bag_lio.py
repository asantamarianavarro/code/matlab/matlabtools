#!/usr/bin/env python
import rosbag
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors

if __name__=='__main__':
    # Read bag file
    # bag = rosbag.Bag('/media/asantamaria/HDDBackup/work/JPL/isrr/stix_army_run2_longfail_2019-04-10-11-50-36_2019-05-22-10-27-59.bag')
    bag = rosbag.Bag('/media/asantamaria/HDDBackup/work/JPL/isrr/tm_lio_stix_miami_run1_2019-05-26-11-52-25.bag')
    vio1_topic = '/rollo/mavros/local_position/odom';
    vio2_topic = '/rollo/qsf/local_position/odom';
    vio1_topic_reinit = '/rollo/orb_slam/reinit';
    vio2_topic_reinit = '/rollo/qsf/vislam/reinit';
    lo_topic = '/rollo/blam_slam/odometry';
    lio_topic = '/rollo/lio/local_position/odom';
    lio_topic_reinit = '/rollo/lio/reinit';

    # Get data from bag files

# PLot 1 LO pose XYZ with reinits (transform manager inputs)
# PLot 2 VIO + LIO pose XYZ with reinits (transform manager output)
# PLot 3 VIO + LIO x velocities pose XYZ with reinits (transform manager output)


    vio1_px = [(t.to_sec(), msg.pose.pose.position.x) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic])]
    t0 = vio1_px[0][0]  # TODO: read this directly to speed up
    vio1_px = [(t - t0, x) for (t, x) in vio1_px]
    lo_px = [(t.to_sec() - t0, msg.pose.pose.position.x) for (topic, msg, t) in bag.read_messages(topics=[lo_topic])]
    lio_px = [(t.to_sec() - t0, msg.pose.pose.position.x) for (topic, msg, t) in bag.read_messages(topics=[lio_topic])]
    lio_reinit = [(t.to_sec() - t0, msg.data) for (topic, msg, t) in bag.read_messages(topics=[lio_topic_reinit])]
    vio1_reinit = [(t.to_sec() - t0, msg.data) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic_reinit])]
    vio2_reinit = [(t.to_sec() - t0, msg.data) for (topic, msg, t) in bag.read_messages(topics=[vio2_topic_reinit])]

    vio1_py = [(t.to_sec(), msg.pose.pose.position.y) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic])]
    t0 = vio1_py[0][0]  # TODO: read this directly to speed up
    vio1_py = [(t - t0, x) for (t, x) in vio1_py]
    lo_py = [(t.to_sec() - t0, msg.pose.pose.position.y) for (topic, msg, t) in bag.read_messages(topics=[lo_topic])]
    lio_py = [(t.to_sec() - t0, msg.pose.pose.position.y) for (topic, msg, t) in bag.read_messages(topics=[lio_topic])]

    vio1_pz = [(t.to_sec(), msg.pose.pose.position.z) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic])]
    t0 = vio1_pz[0][0]  # TODO: read this directly to speed up
    vio1_pz = [(t - t0, x) for (t, x) in vio1_pz]
    lo_pz = [(t.to_sec() - t0, msg.pose.pose.position.z) for (topic, msg, t) in bag.read_messages(topics=[lo_topic])]
    lio_pz = [(t.to_sec() - t0, msg.pose.pose.position.z) for (topic, msg, t) in bag.read_messages(topics=[lio_topic])]

    vio1_vx = [(t.to_sec(), msg.twist.twist.linear.x) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic])]
    t0 = vio1_vx[0][0]  # TODO: read this directly to speed up
    vio1_vx = [(t - t0, x) for (t, x) in vio1_vx]
    vio2_vx = [(t.to_sec() - t0, msg.twist.twist.linear.x) for (topic, msg, t) in bag.read_messages(topics=[vio2_topic])]
    lio_vx = [(t.to_sec() - t0, msg.twist.twist.linear.x) for (topic, msg, t) in bag.read_messages(topics=[lio_topic])]

    vio1_vy = [(t.to_sec(), msg.twist.twist.linear.y) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic])]
    t0 = vio1_vy[0][0]  # TODO: read this directly to speed up
    vio1_vy = [(t - t0, x) for (t, x) in vio1_vy]
    vio2_vy = [(t.to_sec() - t0, msg.twist.twist.linear.y) for (topic, msg, t) in bag.read_messages(topics=[vio2_topic])]
    lio_vy = [(t.to_sec() - t0, msg.twist.twist.linear.y) for (topic, msg, t) in bag.read_messages(topics=[lio_topic])]

    vio1_vz = [(t.to_sec(), msg.twist.twist.linear.z) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic])]
    t0 = vio1_vz[0][0]  # TODO: read this directly to speed up
    vio1_vz = [(t - t0, x) for (t, x) in vio1_vz]
    vio2_vz = [(t.to_sec() - t0, msg.twist.twist.linear.z) for (topic, msg, t) in bag.read_messages(topics=[vio2_topic])]
    lio_vz = [(t.to_sec() - t0, msg.twist.twist.linear.z) for (topic, msg, t) in bag.read_messages(topics=[lio_topic])]

    # Visualize data

    # general params
    plt.rcParams['axes.facecolor'] = 'white'
    plt.rcParams['figure.facecolor'] = 'white'
    plt.rcParams.update({'font.size': 24})
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', weight='light')
    plt.rc('axes', linewidth=1)

    vio1ts = [t[0] for t in vio1_px]
    vio1px = [val[1] for val in vio1_px]
    vio1py = [val[1] for val in vio1_py]
    vio1pz = [val[1] for val in vio1_pz]
    vio1vx = [val[1] for val in vio1_vx]
    vio1vy = [val[1] for val in vio1_vy]
    vio1vz = [val[1] for val in vio1_vz]
    vio2ts = [t[0] for t in vio2_vx]
    vio2vx = [val[1] for val in vio2_vx]
    vio2vy = [val[1] for val in vio2_vy]
    vio2vz = [val[1] for val in vio2_vz]
    lots = [t[0] for t in lo_px]
    lopx = [val[1] for val in lo_px]
    lopy = [val[1] for val in lo_py]
    lopz = [val[1] for val in lo_pz]
    liots = [t[0] for t in lio_px]
    liopx = [val[1] for val in lio_px]
    liopy = [val[1] for val in lio_py]
    liopz = [val[1] for val in lio_pz]
    liovx = [val[1] for val in lio_vx]
    liovy = [val[1] for val in lio_vy]
    liovz = [val[1] for val in lio_vz]
    
    plt.figure(num=1, figsize=(15,5), dpi=100)
    plt.plot(lots, lopx, 'r.', markersize='3')
    plt.plot(lots, lopx, 'rs', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=10)
    plt.plot(lots, lopy, 'g.', markersize='3')
    plt.plot(lots, lopy, 'g^', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=10)
    plt.plot(lots, lopz, 'b.', markersize='3')
    plt.plot(lots, lopz, 'b+', markersize='10', markeredgecolor='b', markerfacecolor='none', markeredgewidth=1.0, markevery=10)

    # plt.plot(*zip(*imu_only), color=(1.0,0.5,0), label='IMU-only', linewidth=2.0)
    plt.xlabel('[s]')
    plt.ylabel('[m]')
    plt.grid(False)
    plt.axis([0, None, None, None])

    # axis
    ax = plt.gca()


    plt.plot([],[], 'rs', label='LO $p_x$', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'g^', label='LO $p_y$', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'b+', label='LO $p_z$', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='b', markerfacecolor='none', markeredgewidth=1.0, markevery=100)

    ## Reinits
    reinits_lio = []
    reinits_lio = [val[0] for val in lio_reinit]

    for ii in range(len(reinits_lio)):
        plt.axvline(x=reinits_lio[ii], label='_nolegend_', linestyle='--', linewidth='1.0', color='k')
        pass
    plt.axvline(x=reinits_lio[0], linestyle='--', linewidth='3.0', color='k', label='LO re-init')


    plt.axis([0, None, 0.0, 3.0])
    
    ax.legend(loc='upper right', fontsize=16)


    plt.tight_layout()


##################################################
##################################################
##################################################

    plt.figure(num=2, figsize=(15,5), dpi=100)
    plt.plot(vio1ts, vio1px, 'r.', markersize='3', linewidth='3')
    plt.plot(vio1ts, vio1px, 'r*', markersize='12', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=120)
    plt.plot(vio1ts, vio1py, 'g.', markersize='3', linewidth='3')
    plt.plot(vio1ts, vio1py, 'gd', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=120)
    plt.plot(vio1ts, vio1pz, 'b.', markersize='3', linewidth='3')
    plt.plot(vio1ts, vio1pz, 'bo', markersize='10', markeredgecolor='b', markerfacecolor='none', markeredgewidth=1.0, markevery=120)

    plt.plot(liots, liopx, 'r.', markersize='3')
    plt.plot(liots, liopx, 'rs', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=120)
    plt.plot(liots, liopy, 'g.', markersize='3')
    plt.plot(liots, liopy, 'g^', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=120)
    plt.plot(liots, liopz, 'b.', markersize='3')
    plt.plot(liots, liopz, 'b+', markersize='10', markeredgecolor='b', markerfacecolor='none', markeredgewidth=1.0, markevery=120)

    # plt.plot(*zip(*imu_only), color=(1.0,0.5,0), label='IMU-only', linewidth=2.0)
    plt.xlabel('[s]')
    plt.ylabel('[m]')
    plt.grid(False)
    plt.axis([0, None, None, None])

    # axis
    ax = plt.gca()

    plt.plot([],[], 'r*', label='VIO $p_x$', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'gd', label='VIO $p_y$', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'bo', label='VIO $p_z$', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='b', markerfacecolor='none', markeredgewidth=1.0, markevery=100)

    plt.plot([],[], 'rs', label='LIO $p_x$', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'g^', label='LIO $p_y$', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'b+', label='LIO $p_z$', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='b', markerfacecolor='none', markeredgewidth=1.0, markevery=100)

    ## Reinits
    reinits_lio = []
    reinits_vio1 = []
    reinits_lio = [val[0] for val in lio_reinit]
    reinits_vio1 = [val[0] for val in vio1_reinit]

    for ii in range(len(reinits_vio1)):
        plt.axvline(x=reinits_vio1[ii], label='_nolegend_', linestyle=':', linewidth='1.0', color='k')
        pass
    plt.axvline(x=reinits_vio1[0], linestyle=':', linewidth='3.0', color='k', label='VIO re-init')

    for ii in range(len(reinits_lio)):
        plt.axvline(x=reinits_lio[ii], label='_nolegend_', linestyle='--', linewidth='1.0', color='k')
        pass
    plt.axvline(x=reinits_lio[0], linestyle='--', linewidth='3.0', color='k', label='LO re-init')

    plt.axis([0, None, None, None])

    ax.legend(loc='upper right', fontsize=16)
    
    plt.tight_layout()


##################################################
##################################################
##################################################

##################################################
##################################################
##################################################

    plt.figure(num=3, figsize=(15,5), dpi=100)
    plt.plot(vio1ts, vio1vx, 'r.', markersize='3', linewidth='3')
    plt.plot(vio1ts, vio1vx, 'rs', markersize='12', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)

    plt.plot(vio2ts, vio2vx, 'g.', markersize='3', linewidth='3')
    plt.plot(vio2ts, vio2vx, 'g^', markersize='12', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)

    plt.plot(liots, liovx, 'b.', markersize='3')
    plt.plot(liots, liovx, 'bo', markersize='10', markeredgecolor='b', markerfacecolor='none', markeredgewidth=1.0, markevery=100)

    # plt.plot(*zip(*imu_only), color=(1.0,0.5,0), label='IMU-only', linewidth=2.0)
    plt.xlabel('[s]')
    plt.ylabel('[m/s]')
    plt.grid(False)
    plt.axis([0, None, None, None])

    # axis
    ax = plt.gca()

    plt.plot([],[], 'rs', label='VIO1 $v_x$', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'g^', label='VIO2 $v_x$', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'bo', label='LIO $v_x$', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='b', markerfacecolor='none', markeredgewidth=1.0, markevery=100)

    ## Reinits
    reinits_vio1 = []
    reinits_vio2 = []
    reinits_lio = []
    reinits_lio = [val[0] for val in lio_reinit]
    reinits_vio1 = [val[0] for val in vio1_reinit]
    reinits_vio2 = [val[0] for val in vio2_reinit]

    for ii in range(len(reinits_vio1)):
        plt.axvline(x=reinits_vio1[ii], label='_nolegend_', linestyle=':', linewidth='1.0', color='k')
        pass
    plt.axvline(x=reinits_vio1[0], linestyle=':', linewidth='3.0', color='k', label='VIO1 re-init')

    for ii in range(len(reinits_vio2)):
        plt.axvline(x=reinits_vio2[ii], label='_nolegend_', linestyle='-', linewidth='1.0', color='k')
        pass
    plt.axvline(x=reinits_vio2[0], linestyle='-', linewidth='1.0', color='k', label='VIO2 re-init')

    for ii in range(len(reinits_lio)):
        plt.axvline(x=reinits_lio[ii], label='_nolegend_', linestyle='--', linewidth='1.0', color='k')
        pass
    plt.axvline(x=reinits_lio[0], linestyle='--', linewidth='3.0', color='k', label='LO re-init')



    plt.axis([0, None, None, None])
    
    ax.legend(loc='upper right', fontsize=16)

    plt.tight_layout()


##################################################
##################################################
##################################################



    plt.show()



