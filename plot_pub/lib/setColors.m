function lineColors=setColors(N,arg)

colors = linspecer(N,arg);
lineColors = num2cell(colors,2);

end