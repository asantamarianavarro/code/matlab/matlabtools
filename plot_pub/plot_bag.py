#!/usr/bin/env python
import rosbag
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors

if __name__=='__main__':
    # Read bag file
    # bag = rosbag.Bag('/media/asantamaria/HDDBackup/work/JPL/isrr/stix_army_run2_longfail_2019-04-10-11-50-36_2019-05-22-10-27-59.bag')
    bag = rosbag.Bag('/media/asantamaria/HDDBackup/work/JPL/isrr/stix_army_run2_2019-05-27-17-50-27.bag')

    vio1_topic = '/rollo/mavros/local_position/odom';
    vio2_topic = '/rollo/qsf/local_position/odom';
    vio1_topic_reinit = '/rollo/orb_slam/reinit';
    vio2_topic_reinit = '/rollo/qsf/vislam/reinit';
    resiliency_topic_status = '/rollo/resiliency_logic/status';
    resiliency_topic_odom = '/rollo/resiliency/odometry';

    # Get data from bag files

    vio1_px = [(t.to_sec(), msg.pose.pose.position.x) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic])]
    t0 = vio1_px[0][0]  # TODO: read this directly to speed up
    vio1_px = [(t - t0, x) for (t, x) in vio1_px]
    vio2_px = [(t.to_sec() - t0, msg.pose.pose.position.x) for (topic, msg, t) in bag.read_messages(topics=[vio2_topic])]
    odom_px = [(t.to_sec() - t0, msg.pose.pose.position.x) for (topic, msg, t) in bag.read_messages(topics=[resiliency_topic_odom])]
    res_channel = [(t.to_sec() - t0, msg.selected_channel) for (topic, msg, t) in bag.read_messages(topics=[resiliency_topic_status])]
    imu_only = [(t.to_sec() - t0, msg.imu_only) for (topic, msg, t) in bag.read_messages(topics=[resiliency_topic_status])]
    vio1_reinit = [(t.to_sec() - t0, msg.data) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic_reinit])]
    vio2_reinit = [(t.to_sec() - t0, msg.data) for (topic, msg, t) in bag.read_messages(topics=[vio2_topic_reinit])]

    vio1_py = [(t.to_sec(), msg.pose.pose.position.y) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic])]
    t0 = vio1_py[0][0]  # TODO: read this directly to speed up
    vio1_py = [(t - t0, x) for (t, x) in vio1_py]
    vio2_py = [(t.to_sec() - t0, msg.pose.pose.position.y) for (topic, msg, t) in bag.read_messages(topics=[vio2_topic])]
    odom_py = [(t.to_sec() - t0, msg.pose.pose.position.y) for (topic, msg, t) in bag.read_messages(topics=[resiliency_topic_odom])]

    vio1_pz = [(t.to_sec(), msg.pose.pose.position.z) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic])]
    t0 = vio1_pz[0][0]  # TODO: read this directly to speed up
    vio1_pz = [(t - t0, x) for (t, x) in vio1_pz]
    vio2_pz = [(t.to_sec() - t0, msg.pose.pose.position.z) for (topic, msg, t) in bag.read_messages(topics=[vio2_topic])]
    odom_pz = [(t.to_sec() - t0, msg.pose.pose.position.z) for (topic, msg, t) in bag.read_messages(topics=[resiliency_topic_odom])]

    vio1_vx = [(t.to_sec(), msg.twist.twist.linear.x) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic])]
    t0 = vio1_vx[0][0]  # TODO: read this directly to speed up
    vio1_vx = [(t - t0, x) for (t, x) in vio1_vx]
    vio2_vx = [(t.to_sec() - t0, msg.twist.twist.linear.x) for (topic, msg, t) in bag.read_messages(topics=[vio2_topic])]
    odom_vx = [(t.to_sec() - t0, msg.twist.twist.linear.x) for (topic, msg, t) in bag.read_messages(topics=[resiliency_topic_odom])]

    vio1_vy = [(t.to_sec(), msg.twist.twist.linear.y) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic])]
    t0 = vio1_vy[0][0]  # TODO: read this directly to speed up
    vio1_vy = [(t - t0, x) for (t, x) in vio1_vy]
    vio2_vy = [(t.to_sec() - t0, msg.twist.twist.linear.y) for (topic, msg, t) in bag.read_messages(topics=[vio2_topic])]
    odom_vy = [(t.to_sec() - t0, msg.twist.twist.linear.y) for (topic, msg, t) in bag.read_messages(topics=[resiliency_topic_odom])]

    vio1_vz = [(t.to_sec(), msg.twist.twist.linear.z) for (topic, msg, t) in bag.read_messages(topics=[vio1_topic])]
    t0 = vio1_vz[0][0]  # TODO: read this directly to speed up
    vio1_vz = [(t - t0, x) for (t, x) in vio1_vz]
    vio2_vz = [(t.to_sec() - t0, msg.twist.twist.linear.z) for (topic, msg, t) in bag.read_messages(topics=[vio2_topic])]
    odom_vz = [(t.to_sec() - t0, msg.twist.twist.linear.z) for (topic, msg, t) in bag.read_messages(topics=[resiliency_topic_odom])]


    # Change channel to match resiliency output namings
    channel = []
    for ii in range(len(res_channel)):
        val = []
        if (res_channel[ii][1] == 1.0) :
            val = 2.0
        else :
            val = 1.0
        pass
        channel.append([res_channel[ii][0], val])
        pass


    # Create new variables to plot mobility phase
    # Create new variables to plot phase
    mobility = []
    tcount = 0
    for ii in range(len(imu_only)) :
        if imu_only[ii][1] == 1.0 :
          if imu_only[ii][0] - tcount < 3.0 :
            mobility.append(1.5)
          else :
            mobility.append(0.5)
        else :  
          tcount = imu_only[ii][0]
          mobility.append(3.0)
    mobilityts = [t[0] for t in imu_only]


    # Visualize data

    # general params
    plt.rcParams['axes.facecolor'] = 'white'
    plt.rcParams['figure.facecolor'] = 'white'
    plt.rcParams.update({'font.size': 24})
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', weight='light')
    plt.rc('axes', linewidth=1)


    vio1ts = [t[0] for t in vio1_px]
    vio1px = [val[1] for val in vio1_px]
    vio1py = [val[1] for val in vio1_py]
    vio1pz = [val[1] for val in vio1_pz]
    vio1vx = [val[1] for val in vio1_vx]
    vio1vy = [val[1] for val in vio1_vy]
    vio1vz = [val[1] for val in vio1_vz]
    vio2ts = [t[0] for t in vio2_px]
    vio2px = [val[1] for val in vio2_px]
    vio2py = [val[1] for val in vio2_py]
    vio2pz = [val[1] for val in vio2_pz]
    vio2vx = [val[1] for val in vio2_vx]
    vio2vy = [val[1] for val in vio2_vy]
    vio2vz = [val[1] for val in vio2_vz]
    odomts = [t[0] for t in odom_px]
    odompx = [val[1] for val in odom_px]
    odompy = [val[1] for val in odom_py]
    odompz = [val[1] for val in odom_pz]
    odomvx = [val[1] for val in odom_vx]
    odomvy = [val[1] for val in odom_vy]
    odomvz = [val[1] for val in odom_vz]





    plt.figure(num=1, figsize=(15,5), dpi=100)
    plt.plot(vio1ts, vio1px, 'r.', markersize='3')
    plt.plot(vio1ts, vio1px, 'rs', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot(vio2ts, vio2px, 'g.', markersize='3')
    plt.plot(vio2ts, vio2px, 'g^', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot(odomts, odompx, 'b.', linewidth=1.0)
    # plt.plot(*zip(*imu_only), color=(1.0,0.5,0), label='IMU-only', linewidth=2.0)
    plt.xlabel('[s]')
    plt.ylabel('[m]')
    plt.grid(False)
    plt.axis([0, None, None, None])

    # axis
    ax = plt.gca()

    # secondary axis
    ay2 = ax.twinx()

    ay2.set_ylabel('Selected channel', color='m')
    # ay2.plot(mobilityts, mobility, label='_nolegend_', color=(1.0,0.5,0), linewidth=1.0)
    plt.plot(*zip(*channel), label='_nolegend_', color='m', linestyle='-', linewidth=2.0)
    # plt.text(161, 2.0, 'VIO2', fontsize=14, fontweight='bold', color='m')
    # plt.text(161, 1.0, 'VIO1', fontsize=14, fontweight='bold', color='m')

     # Mobility areas
    green_ini=[] 
    green_end=[] 
    orange_ini=[] 
    orange_end=[] 
    red_ini=[] 
    red_end=[] 
    green_on=True
    orange_on=False
    red_on=False
    green_ini.append(mobilityts[0])
    val_old=mobility[0]
    for ii in range(len(mobilityts)) :
        if val_old != mobility[ii]:
            if green_on :
                green_on=False
                green_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 1.5 :
                    orange_on=True
                    orange_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 0.5:
                    red_on=True
                    red_ini.append(mobilityts[ii])
                    pass
                pass    
            elif orange_on:
                orange_on=False
                orange_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 3.0 :
                    green_on=True
                    green_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 0.5:
                    red_on=True
                    red_ini.append(mobilityts[ii])
                    pass
                pass    
            elif red_on:
                red_on=False
                red_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 3.0 :
                    green_on=True
                    green_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 1.5:
                    orange_on=True
                    orange_ini.append(mobility[ii])
                    pass
                pass
            pass
        pass         
    if green_on :
        green_end.append(mobilityts[-1])
        pass
    if orange_on :
        orange_end.append(mobilityts[-1])
        pass
    if red_on :
        red_end.append(mobilityts[-1])
        pass
       

    for ii in range(len(green_ini)) :
      ay2.axvspan(green_ini[ii], green_end[ii], ymin=0.0, ymax=1.0, alpha=0.2, facecolor='g', edgecolor='None', label='_nolegend_')
    for ii in range(len(orange_ini)) :
      ay2.axvspan(orange_ini[ii], orange_end[ii], ymin=0.0, ymax=1.0, alpha=0.4, facecolor=(1.0,0.5,0.0), edgecolor='None', label='_nolegend_')
      # ay2.axvspan(orange_ini[ii], orange_end[ii], ymin=0.0, ymax=0.5, alpha=0.4, facecolor=(1.0,0.5,0.0), edgecolor='None', label='_nolegend_')
    for ii in range(len(red_ini)) :
      ay2.axvspan(red_ini[ii], red_end[ii], ymin=0.0, ymax=1.0, alpha=0.2, facecolor='r', edgecolor='None', label='_nolegend_')
      # ay2.axvspan(red_ini[ii], red_end[ii], ymin=0.0, ymax=0.1666, alpha=0.2, facecolor='r', edgecolor='None', label='_nolegend_')


    # ay2.set_yticks([0.5, 1.5, 3.0])
    # ay2.set_yticklabels(['Att', 'Local', 'Global'])
    # ay2.get_yticklabels()[0].set_color("red")
    # ay2.get_yticklabels()[1].set_color((1.0,0.5,0.0))
    # ay2.get_yticklabels()[2].set_color("green")
    ay2.set_yticks([1.0, 2.0])
    # ay2.set_yticklabels(['VIO1', 'VIO2'])
    ay2.set_yticklabels(['VIO1', 'VIO2'], color='m')


    plt.plot([],[], 'rs', label='VIO1', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'g^', label='VIO2', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'b', label='Resilient output', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], color='m', label='Selected channel', linestyle='-', linewidth=2.0)


    ## Reinits
    reinits_vio1 = []
    reinits_vio2 = []
    reinits_vio1 = [val[0] for val in vio1_reinit]
    reinits_vio2 = [val[0] for val in vio2_reinit]

    for ii in range(len(reinits_vio1)):
        plt.axvline(x=reinits_vio1[ii], label='_nolegend_', marker='s', markersize='10.0', linestyle='-', linewidth='1.0', color='k')
        pass
    for ii in range(len(reinits_vio2)):
        plt.axvline(x=reinits_vio2[ii], label='_nolegend_', marker='^', markersize='10.0', linestyle='-.', linewidth='2.0', color='k')
        pass
    plt.axvline(x=reinits_vio1[0], marker='s', markersize='10.0', linestyle='-', linewidth='1.0', color='k', label='VIO1 re-init')
    plt.axvline(x=reinits_vio2[0], marker='^', markersize='10.0', linestyle='-.', linewidth='2.0', color='k', label='VIO2 re-init')


    ay2.legend(loc='upper right', fontsize=16)

    plt.axis([0, None, 0.0, 3.0])
    
    plt.tight_layout()


##################################################
##################################################
##################################################




    plt.figure(num=2, figsize=(15,5), dpi=100)
    plt.plot(vio1ts, vio1py, 'r.', markersize='3')
    plt.plot(vio1ts, vio1py, 'rs', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot(vio2ts, vio2py, 'g.', markersize='3')
    plt.plot(vio2ts, vio2py, 'g^', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot(odomts, odompy, 'b.', linewidth=1.0)
    # plt.plot(*zip(*imu_only), color=(1.0,0.5,0), label='IMU-only', linewidth=2.0)
    plt.xlabel('[s]')
    plt.ylabel('[m]')
    plt.grid(False)
    plt.axis([0, None, None, None])

    # axis
    ax = plt.gca()

    # secondary axis
    ay2 = ax.twinx()

    ay2.set_ylabel('Selected channel', color='m')
    # ay2.plot(mobilityts, mobility, label='_nolegend_', color=(1.0,0.5,0), linewidth=1.0)
    plt.plot(*zip(*channel), label='_nolegend_', color='m', linestyle='-', linewidth=2.0)
    # plt.text(161, 2.0, 'VIO2', fontsize=14, fontweight='bold', color='m')
    # plt.text(161, 1.0, 'VIO1', fontsize=14, fontweight='bold', color='m')

     # Mobility areas
    green_ini=[] 
    green_end=[] 
    orange_ini=[] 
    orange_end=[] 
    red_ini=[] 
    red_end=[] 
    green_on=True
    orange_on=False
    red_on=False
    green_ini.append(mobilityts[0])
    val_old=mobility[0]
    for ii in range(len(mobilityts)) :
        if val_old != mobility[ii]:
            if green_on :
                green_on=False
                green_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 1.5 :
                    orange_on=True
                    orange_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 0.5:
                    red_on=True
                    red_ini.append(mobilityts[ii])
                    pass
                pass    
            elif orange_on:
                orange_on=False
                orange_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 3.0 :
                    green_on=True
                    green_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 0.5:
                    red_on=True
                    red_ini.append(mobilityts[ii])
                    pass
                pass    
            elif red_on:
                red_on=False
                red_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 3.0 :
                    green_on=True
                    green_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 1.5:
                    orange_on=True
                    orange_ini.append(mobility[ii])
                    pass
                pass
            pass
        pass         
    if green_on :
        green_end.append(mobilityts[-1])
        pass
    if orange_on :
        orange_end.append(mobilityts[-1])
        pass
    if red_on :
        red_end.append(mobilityts[-1])
        pass
       

    for ii in range(len(green_ini)) :
      ay2.axvspan(green_ini[ii], green_end[ii], ymin=0.0, ymax=1.0, alpha=0.2, facecolor='g', edgecolor='None', label='_nolegend_')
    for ii in range(len(orange_ini)) :
      ay2.axvspan(orange_ini[ii], orange_end[ii], ymin=0.0, ymax=1.0, alpha=0.4, facecolor=(1.0,0.5,0.0), edgecolor='None', label='_nolegend_')
      # ay2.axvspan(orange_ini[ii], orange_end[ii], ymin=0.0, ymax=0.5, alpha=0.4, facecolor=(1.0,0.5,0.0), edgecolor='None', label='_nolegend_')
    for ii in range(len(red_ini)) :
      ay2.axvspan(red_ini[ii], red_end[ii], ymin=0.0, ymax=1.0, alpha=0.2, facecolor='r', edgecolor='None', label='_nolegend_')
      # ay2.axvspan(red_ini[ii], red_end[ii], ymin=0.0, ymax=0.1666, alpha=0.2, facecolor='r', edgecolor='None', label='_nolegend_')


    # ay2.set_yticks([0.5, 1.5, 3.0])
    # ay2.set_yticklabels(['Att', 'Local', 'Global'])
    # ay2.get_yticklabels()[0].set_color("red")
    # ay2.get_yticklabels()[1].set_color((1.0,0.5,0.0))
    # ay2.get_yticklabels()[2].set_color("green")
    ay2.set_yticks([1.0, 2.0])
    ay2.set_yticklabels(['VIO1', 'VIO2'], color='m')

    plt.plot([],[], 'rs', label='VIO1', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'g^', label='VIO2', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'b', label='Resilient output', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], color='m', label='Selected channel', linestyle='-', linewidth=2.0)


    ## Reinits
    reinits_vio1 = []
    reinits_vio2 = []
    reinits_vio1 = [val[0] for val in vio1_reinit]
    reinits_vio2 = [val[0] for val in vio2_reinit]

    for ii in range(len(reinits_vio1)):
        plt.axvline(x=reinits_vio1[ii], label='_nolegend_', marker='s', markersize='10.0', linestyle='-', linewidth='1.0', color='k')
        pass
    for ii in range(len(reinits_vio2)):
        plt.axvline(x=reinits_vio2[ii], label='_nolegend_', marker='^', markersize='10.0', linestyle='-.', linewidth='2.0', color='k')
        pass
    plt.axvline(x=reinits_vio1[0], marker='s', markersize='10.0', linestyle='-', linewidth='1.0', color='k', label='VIO1 re-init')
    plt.axvline(x=reinits_vio2[0], marker='^', markersize='10.0', linestyle='-.', linewidth='2.0', color='k', label='VIO2 re-init')


    ay2.legend(loc='upper right', fontsize=16)

    plt.axis([0, None, 0.0, 3.0])
    
    plt.tight_layout()


##################################################
##################################################
##################################################





    plt.figure(num=3, figsize=(15,5), dpi=100)
    plt.plot(vio1ts, vio1pz, 'r.', markersize='3')
    plt.plot(vio1ts, vio1pz, 'rs', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot(vio2ts, vio2pz, 'g.', markersize='3')
    plt.plot(vio2ts, vio2pz, 'g^', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot(odomts, odompz, 'b.', linewidth=1.0)
    # plt.plot(*zip(*imu_only), color=(1.0,0.5,0), label='IMU-only', linewidth=2.0)
    plt.xlabel('[s]')
    plt.ylabel('[m]')
    plt.grid(False)
    plt.axis([0, None, None, None])

    # axis
    ax = plt.gca()

    # secondary axis
    ay2 = ax.twinx()

    ay2.set_ylabel('Selected channel', color='m')
    # ay2.plot(mobilityts, mobility, label='_nolegend_', color=(1.0,0.5,0), linewidth=1.0)
    plt.plot(*zip(*channel), label='_nolegend_', color='m', linestyle='-', linewidth=2.0)
    # plt.text(161, 2.0, 'VIO2', fontsize=14, fontweight='bold', color='m')
    # plt.text(161, 1.0, 'VIO1', fontsize=14, fontweight='bold', color='m')

     # Mobility areas
    green_ini=[] 
    green_end=[] 
    orange_ini=[] 
    orange_end=[] 
    red_ini=[] 
    red_end=[] 
    green_on=True
    orange_on=False
    red_on=False
    green_ini.append(mobilityts[0])
    val_old=mobility[0]
    for ii in range(len(mobilityts)) :
        if val_old != mobility[ii]:
            if green_on :
                green_on=False
                green_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 1.5 :
                    orange_on=True
                    orange_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 0.5:
                    red_on=True
                    red_ini.append(mobilityts[ii])
                    pass
                pass    
            elif orange_on:
                orange_on=False
                orange_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 3.0 :
                    green_on=True
                    green_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 0.5:
                    red_on=True
                    red_ini.append(mobilityts[ii])
                    pass
                pass    
            elif red_on:
                red_on=False
                red_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 3.0 :
                    green_on=True
                    green_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 1.5:
                    orange_on=True
                    orange_ini.append(mobility[ii])
                    pass
                pass
            pass
        pass         
    if green_on :
        green_end.append(mobilityts[-1])
        pass
    if orange_on :
        orange_end.append(mobilityts[-1])
        pass
    if red_on :
        red_end.append(mobilityts[-1])
        pass
       

    for ii in range(len(green_ini)) :
      ay2.axvspan(green_ini[ii], green_end[ii], ymin=0.0, ymax=1.0, alpha=0.2, facecolor='g', edgecolor='None', label='_nolegend_')
    for ii in range(len(orange_ini)) :
      ay2.axvspan(orange_ini[ii], orange_end[ii], ymin=0.0, ymax=1.0, alpha=0.4, facecolor=(1.0,0.5,0.0), edgecolor='None', label='_nolegend_')
      # ay2.axvspan(orange_ini[ii], orange_end[ii], ymin=0.0, ymax=0.5, alpha=0.4, facecolor=(1.0,0.5,0.0), edgecolor='None', label='_nolegend_')
    for ii in range(len(red_ini)) :
      ay2.axvspan(red_ini[ii], red_end[ii], ymin=0.0, ymax=1.0, alpha=0.2, facecolor='r', edgecolor='None', label='_nolegend_')
      # ay2.axvspan(red_ini[ii], red_end[ii], ymin=0.0, ymax=0.1666, alpha=0.2, facecolor='r', edgecolor='None', label='_nolegend_')


    # ay2.set_yticks([0.5, 1.5, 3.0])
    # ay2.set_yticklabels(['Att', 'Local', 'Global'])
    # ay2.get_yticklabels()[0].set_color("red")
    # ay2.get_yticklabels()[1].set_color((1.0,0.5,0.0))
    # ay2.get_yticklabels()[2].set_color("green")
    ay2.set_yticks([1.0, 2.0])    
    ay2.set_yticklabels(['VIO1', 'VIO2'], color='m')

    plt.plot([],[], 'rs', label='VIO1', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'g^', label='VIO2', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'b', label='Resilient output', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], color='m', label='Selected channel', linestyle='-', linewidth=2.0)


    ## Reinits
    reinits_vio1 = []
    reinits_vio2 = []
    reinits_vio1 = [val[0] for val in vio1_reinit]
    reinits_vio2 = [val[0] for val in vio2_reinit]

    for ii in range(len(reinits_vio1)):
        plt.axvline(x=reinits_vio1[ii], label='_nolegend_', marker='s', markersize='10.0', linestyle='-', linewidth='1.0', color='k')
        pass
    for ii in range(len(reinits_vio2)):
        plt.axvline(x=reinits_vio2[ii], label='_nolegend_', marker='^', markersize='10.0', linestyle='-.', linewidth='2.0', color='k')
        pass
    plt.axvline(x=reinits_vio1[0], marker='s', markersize='10.0', linestyle='-', linewidth='1.0', color='k', label='VIO1 re-init')
    plt.axvline(x=reinits_vio2[0], marker='^', markersize='10.0', linestyle='-.', linewidth='2.0', color='k', label='VIO2 re-init')


    ay2.legend(loc='upper right', fontsize=16)

    plt.axis([0, None, 0.0, 3.0])
    
    plt.tight_layout()


##################################################
##################################################
##################################################





    plt.figure(num=4, figsize=(15,5), dpi=100)
    plt.plot(vio1ts, vio1vx, 'r.', markersize='3')
    plt.plot(vio1ts, vio1vx, 'rs', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot(vio2ts, vio2vx, 'g.', markersize='3')
    plt.plot(vio2ts, vio2vx, 'g^', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot(odomts, odomvx, 'b.', linewidth=1.0)
    # plt.plot(*zip(*imu_only), color=(1.0,0.5,0), label='IMU-only', linewidth=2.0)
    plt.xlabel('[s]')
    plt.ylabel('[m/s]')
    plt.grid(False)
    plt.axis([0, None, None, None])

    # axis
    ax = plt.gca()

    # secondary axis
    ay2 = ax.twinx()

    ay2.set_ylabel('Selected channel', color='m')
    # ay2.plot(mobilityts, mobility, label='_nolegend_', color=(1.0,0.5,0), linewidth=1.0)
    plt.plot(*zip(*channel), label='_nolegend_', color='m', linestyle='-', linewidth=2.0)
    # plt.text(161, 2.0, 'VIO2', fontsize=14, fontweight='bold', color='m')
    # plt.text(161, 1.0, 'VIO1', fontsize=14, fontweight='bold', color='m')

     # Mobility areas
    green_ini=[] 
    green_end=[] 
    orange_ini=[] 
    orange_end=[] 
    red_ini=[] 
    red_end=[] 
    green_on=True
    orange_on=False
    red_on=False
    green_ini.append(mobilityts[0])
    val_old=mobility[0]
    for ii in range(len(mobilityts)) :
        if val_old != mobility[ii]:
            if green_on :
                green_on=False
                green_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 1.5 :
                    orange_on=True
                    orange_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 0.5:
                    red_on=True
                    red_ini.append(mobilityts[ii])
                    pass
                pass    
            elif orange_on:
                orange_on=False
                orange_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 3.0 :
                    green_on=True
                    green_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 0.5:
                    red_on=True
                    red_ini.append(mobilityts[ii])
                    pass
                pass    
            elif red_on:
                red_on=False
                red_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 3.0 :
                    green_on=True
                    green_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 1.5:
                    orange_on=True
                    orange_ini.append(mobility[ii])
                    pass
                pass
            pass
        pass         
    if green_on :
        green_end.append(mobilityts[-1])
        pass
    if orange_on :
        orange_end.append(mobilityts[-1])
        pass
    if red_on :
        red_end.append(mobilityts[-1])
        pass
       

    for ii in range(len(green_ini)) :
      ay2.axvspan(green_ini[ii], green_end[ii], ymin=0.0, ymax=1.0, alpha=0.2, facecolor='g', edgecolor='None', label='_nolegend_')
    for ii in range(len(orange_ini)) :
      ay2.axvspan(orange_ini[ii], orange_end[ii], ymin=0.0, ymax=1.0, alpha=0.4, facecolor=(1.0,0.5,0.0), edgecolor='None', label='_nolegend_')
      # ay2.axvspan(orange_ini[ii], orange_end[ii], ymin=0.0, ymax=0.5, alpha=0.4, facecolor=(1.0,0.5,0.0), edgecolor='None', label='_nolegend_')
    for ii in range(len(red_ini)) :
      ay2.axvspan(red_ini[ii], red_end[ii], ymin=0.0, ymax=1.0, alpha=0.2, facecolor='r', edgecolor='None', label='_nolegend_')
      # ay2.axvspan(red_ini[ii], red_end[ii], ymin=0.0, ymax=0.1666, alpha=0.2, facecolor='r', edgecolor='None', label='_nolegend_')


    # ay2.set_yticks([0.5, 1.5, 3.0])
    # ay2.set_yticklabels(['Att', 'Local', 'Global'])
    # ay2.get_yticklabels()[0].set_color("red")
    # ay2.get_yticklabels()[1].set_color((1.0,0.5,0.0))
    # ay2.get_yticklabels()[2].set_color("green")
    ay2.set_yticks([1.0, 2.0])    
    ay2.set_yticklabels(['VIO1', 'VIO2'], color='m')


    plt.plot([],[], 'rs', label='VIO1', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'g^', label='VIO2', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'b', label='Resilient output', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], color='m', label='Selected channel', linestyle='-', linewidth=2.0)


    ## Reinits
    reinits_vio1 = []
    reinits_vio2 = []
    reinits_vio1 = [val[0] for val in vio1_reinit]
    reinits_vio2 = [val[0] for val in vio2_reinit]

    for ii in range(len(reinits_vio1)):
        plt.axvline(x=reinits_vio1[ii], label='_nolegend_', marker='s', markersize='10.0', linestyle='-', linewidth='1.0', color='k')
        pass
    for ii in range(len(reinits_vio2)):
        plt.axvline(x=reinits_vio2[ii], label='_nolegend_', marker='^', markersize='10.0', linestyle='-.', linewidth='2.0', color='k')
        pass
    plt.axvline(x=reinits_vio1[0], marker='s', markersize='10.0', linestyle='-', linewidth='1.0', color='k', label='VIO1 re-init')
    plt.axvline(x=reinits_vio2[0], marker='^', markersize='10.0', linestyle='-.', linewidth='2.0', color='k', label='VIO2 re-init')


    ay2.legend(loc='upper right', fontsize=16)

    plt.axis([0, None, 0.0, 3.0])
    
    plt.tight_layout()


##################################################
##################################################
##################################################




    plt.figure(num=5, figsize=(15,5), dpi=100)
    plt.plot(vio1ts, vio1vy, 'r.', markersize='3')
    plt.plot(vio1ts, vio1vy, 'rs', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot(vio2ts, vio2vy, 'g.', markersize='3')
    plt.plot(vio2ts, vio2vy, 'g^', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot(odomts, odomvy, 'b.', linewidth=1.0)
    # plt.plot(*zip(*imu_only), color=(1.0,0.5,0), label='IMU-only', linewidth=2.0)
    plt.xlabel('[s]')
    plt.ylabel('[m/s]')
    plt.grid(False)
    plt.axis([0, None, None, None])

    # axis
    ax = plt.gca()

    # secondary axis
    ay2 = ax.twinx()

    ay2.set_ylabel('Selected channel', color='m')
    # ay2.plot(mobilityts, mobility, label='_nolegend_', color=(1.0,0.5,0), linewidth=1.0)
    plt.plot(*zip(*channel), label='_nolegend_', color='m', linestyle='-', linewidth=2.0)
    # plt.text(161, 2.0, 'VIO2', fontsize=14, fontweight='bold', color='m')
    # plt.text(161, 1.0, 'VIO1', fontsize=14, fontweight='bold', color='m')

     # Mobility areas
    green_ini=[] 
    green_end=[] 
    orange_ini=[] 
    orange_end=[] 
    red_ini=[] 
    red_end=[] 
    green_on=True
    orange_on=False
    red_on=False
    green_ini.append(mobilityts[0])
    val_old=mobility[0]
    for ii in range(len(mobilityts)) :
        if val_old != mobility[ii]:
            if green_on :
                green_on=False
                green_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 1.5 :
                    orange_on=True
                    orange_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 0.5:
                    red_on=True
                    red_ini.append(mobilityts[ii])
                    pass
                pass    
            elif orange_on:
                orange_on=False
                orange_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 3.0 :
                    green_on=True
                    green_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 0.5:
                    red_on=True
                    red_ini.append(mobilityts[ii])
                    pass
                pass    
            elif red_on:
                red_on=False
                red_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 3.0 :
                    green_on=True
                    green_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 1.5:
                    orange_on=True
                    orange_ini.append(mobility[ii])
                    pass
                pass
            pass
        pass         
    if green_on :
        green_end.append(mobilityts[-1])
        pass
    if orange_on :
        orange_end.append(mobilityts[-1])
        pass
    if red_on :
        red_end.append(mobilityts[-1])
        pass
       

    for ii in range(len(green_ini)) :
      ay2.axvspan(green_ini[ii], green_end[ii], ymin=0.0, ymax=1.0, alpha=0.2, facecolor='g', edgecolor='None', label='_nolegend_')
    for ii in range(len(orange_ini)) :
      ay2.axvspan(orange_ini[ii], orange_end[ii], ymin=0.0, ymax=1.0, alpha=0.4, facecolor=(1.0,0.5,0.0), edgecolor='None', label='_nolegend_')
      # ay2.axvspan(orange_ini[ii], orange_end[ii], ymin=0.0, ymax=0.5, alpha=0.4, facecolor=(1.0,0.5,0.0), edgecolor='None', label='_nolegend_')
    for ii in range(len(red_ini)) :
      ay2.axvspan(red_ini[ii], red_end[ii], ymin=0.0, ymax=1.0, alpha=0.2, facecolor='r', edgecolor='None', label='_nolegend_')
      # ay2.axvspan(red_ini[ii], red_end[ii], ymin=0.0, ymax=0.1666, alpha=0.2, facecolor='r', edgecolor='None', label='_nolegend_')


    # ay2.set_yticks([0.5, 1.5, 3.0])
    # ay2.set_yticklabels(['Att', 'Local', 'Global'])
    # ay2.get_yticklabels()[0].set_color("red")
    # ay2.get_yticklabels()[1].set_color((1.0,0.5,0.0))
    # ay2.get_yticklabels()[2].set_color("green")
    ay2.set_yticks([1.0, 2.0])    
    ay2.set_yticklabels(['VIO1', 'VIO2'], color='m')


    plt.plot([],[], 'rs', label='VIO1', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'g^', label='VIO2', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'b', label='Resilient output', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], color='m', label='Selected channel', linestyle='-', linewidth=2.0)


    ## Reinits
    reinits_vio1 = []
    reinits_vio2 = []
    reinits_vio1 = [val[0] for val in vio1_reinit]
    reinits_vio2 = [val[0] for val in vio2_reinit]

    for ii in range(len(reinits_vio1)):
        plt.axvline(x=reinits_vio1[ii], label='_nolegend_', marker='s', markersize='10.0', linestyle='-', linewidth='1.0', color='k')
        pass
    for ii in range(len(reinits_vio2)):
        plt.axvline(x=reinits_vio2[ii], label='_nolegend_', marker='^', markersize='10.0', linestyle='-.', linewidth='2.0', color='k')
        pass
    plt.axvline(x=reinits_vio1[0], marker='s', markersize='10.0', linestyle='-', linewidth='1.0', color='k', label='VIO1 re-init')
    plt.axvline(x=reinits_vio2[0], marker='^', markersize='10.0', linestyle='-.', linewidth='2.0', color='k', label='VIO2 re-init')


    ay2.legend(loc='upper right', fontsize=16)

    plt.axis([0, None, 0.0, 3.0])
    
    plt.tight_layout()


##################################################
##################################################
##################################################




    plt.figure(num=6, figsize=(15,5), dpi=100)
    plt.plot(vio1ts, vio1vz, 'r.', markersize='3')
    plt.plot(vio1ts, vio1vz, 'rs', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot(vio2ts, vio2vz, 'g.', markersize='3')
    plt.plot(vio2ts, vio2vz, 'g^', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot(odomts, odomvz, 'b.', linewidth=1.0)
    # plt.plot(*zip(*imu_only), color=(1.0,0.5,0), label='IMU-only', linewidth=2.0)
    plt.xlabel('[s]')
    plt.ylabel('[m/s]')
    plt.grid(False)
    plt.axis([0, None, None, None])

    # axis
    ax = plt.gca()

    # secondary axis
    ay2 = ax.twinx()

    ay2.set_ylabel('Selected channel', color='m')
    # ay2.plot(mobilityts, mobility, label='_nolegend_', color=(1.0,0.5,0), linewidth=1.0)
    plt.plot(*zip(*channel), label='_nolegend_', color='m', linestyle='-', linewidth=2.0)
    # plt.text(161, 2.0, 'VIO2', fontsize=14, fontweight='bold', color='m')
    # plt.text(161, 1.0, 'VIO1', fontsize=14, fontweight='bold', color='m')

     # Mobility areas
    green_ini=[] 
    green_end=[] 
    orange_ini=[] 
    orange_end=[] 
    red_ini=[] 
    red_end=[] 
    green_on=True
    orange_on=False
    red_on=False
    green_ini.append(mobilityts[0])
    val_old=mobility[0]
    for ii in range(len(mobilityts)) :
        if val_old != mobility[ii]:
            if green_on :
                green_on=False
                green_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 1.5 :
                    orange_on=True
                    orange_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 0.5:
                    red_on=True
                    red_ini.append(mobilityts[ii])
                    pass
                pass    
            elif orange_on:
                orange_on=False
                orange_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 3.0 :
                    green_on=True
                    green_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 0.5:
                    red_on=True
                    red_ini.append(mobilityts[ii])
                    pass
                pass    
            elif red_on:
                red_on=False
                red_end.append(mobilityts[ii])
                val_old=mobility[ii]
                if mobility[ii] == 3.0 :
                    green_on=True
                    green_ini.append(mobilityts[ii])
                    pass
                elif mobility[ii] == 1.5:
                    orange_on=True
                    orange_ini.append(mobility[ii])
                    pass
                pass
            pass
        pass         
    if green_on :
        green_end.append(mobilityts[-1])
        pass
    if orange_on :
        orange_end.append(mobilityts[-1])
        pass
    if red_on :
        red_end.append(mobilityts[-1])
        pass
       

    for ii in range(len(green_ini)) :
      ay2.axvspan(green_ini[ii], green_end[ii], ymin=0.0, ymax=1.0, alpha=0.2, facecolor='g', edgecolor='None', label='_nolegend_')
    for ii in range(len(orange_ini)) :
      ay2.axvspan(orange_ini[ii], orange_end[ii], ymin=0.0, ymax=1.0, alpha=0.4, facecolor=(1.0,0.5,0.0), edgecolor='None', label='_nolegend_')
    for ii in range(len(red_ini)) :
      ay2.axvspan(red_ini[ii], red_end[ii], ymin=0.0, ymax=1.0, alpha=0.2, facecolor='r', edgecolor='None', label='_nolegend_')


    # ay2.set_yticks([0.5, 1.5, 3.0])
    # ay2.set_yticklabels(['Att', 'Local', 'Global'])
    # ay2.get_yticklabels()[0].set_color("red")
    # ay2.get_yticklabels()[1].set_color((1.0,0.5,0.0))
    # ay2.get_yticklabels()[2].set_color("green")
    ay2.set_yticks([1.0, 2.0])    
    ay2.set_yticklabels(['VIO1', 'VIO2'], color='m')

    plt.plot([],[], 'rs', label='VIO1', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='r', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'g^', label='VIO2', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], 'b', label='Resilient output', linestyle='-', linewidth='3.0', markersize='10', markeredgecolor='g', markerfacecolor='none', markeredgewidth=1.0, markevery=100)
    plt.plot([],[], color='m', label='Selected channel', linestyle='-', linewidth=2.0)


    ## Reinits
    reinits_vio1 = []
    reinits_vio2 = []
    reinits_vio1 = [val[0] for val in vio1_reinit]
    reinits_vio2 = [val[0] for val in vio2_reinit]

    for ii in range(len(reinits_vio1)):
        plt.axvline(x=reinits_vio1[ii], label='_nolegend_', marker='s', markersize='10.0', linestyle='-', linewidth='1.0', color='k')
        pass
    for ii in range(len(reinits_vio2)):
        plt.axvline(x=reinits_vio2[ii], label='_nolegend_', marker='^', markersize='10.0', linestyle='-.', linewidth='2.0', color='k')
        pass
    plt.axvline(x=reinits_vio1[0], marker='s', markersize='10.0', linestyle='-', linewidth='1.0', color='k', label='VIO1 re-init')
    plt.axvline(x=reinits_vio2[0], marker='^', markersize='10.0', linestyle='-.', linewidth='2.0', color='k', label='VIO2 re-init')


    ay2.legend(loc='upper right', fontsize=16)

    plt.axis([0, None, 0.0, 3.0])
    
    plt.tight_layout()


##################################################
##################################################
##################################################


    plt.show()



