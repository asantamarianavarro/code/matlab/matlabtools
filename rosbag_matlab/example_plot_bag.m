%  \file read_rosbag.m
% 
%  Created on: 23/07/2017
%      \author: AtDinesh

clear rosbag_wrapper;
clear ros.Bag;
clear all

% Add paths
addpath(genpath(strcat(pwd,'/matlab_rosbag-0.4.1-linux64')));
addpath(genpath(strcat(pwd,'/functions')));
addpath('/rotations');

% bag name
%bagfile='/bagfiles/all_sspiral/all_sspiral_2017-07-22-13-47-11/all_sspiral_2017-07-22-13-47-11.bag';
bagfile='/bagfiles/all_sspiral/all_sspiral_2017-07-22-13-47-11/all_sspiral_2017-07-22-13-47-11.bag';

% Load bagfile
bag = ros.Bag.load(strcat(pwd,bagfile));
%clear bagfile;

% Load IMU mpu6050
display('Loading IMU mpu6050 data...');
imu_mpu6050 = imu_from_rosbag(bag,'/imu_mpu6050');

% Load right pose
display('Loading Right pose data...');
right_pose = pose_from_rosbag(bag,'/right_foot/pose');

display('Writting data files ...');
%% format IMU data matrix as [ts a w]
mpu6050_data = [imu_mpu6050.ts' imu_mpu6050.a' imu_mpu6050.w'];

%% compute odometry from right poses
nb_meas = length(right_pose.p);

right_t_odom = [right_pose.ts(2:nb_meas)];
right_p_odom = [];

for i = 1:1:nb_meas-1
    tmp_p = right_pose.p(:,i+1) - right_pose.p(:,i); %expressed in Mocap frame
    tmp_p = qRot(tmp_p, q2qc(right_pose.q(:,i))); %expressed in IMU local frame
    right_p_odom = [right_p_odom tmp_p];
end

right_o_odom = [];
for i = 1:1:nb_meas-1
    tmp_o = qProd(q2qc(right_pose.q(:,i)), right_pose.q(:,i+1));  %expressed
    right_o_odom = [right_o_odom q2v(tmp_o)];
end

right_odom = [right_t_odom' right_p_odom' right_o_odom'];

%% add noise
right_odom_n = [right_t_odom' right_p_odom' right_o_odom'];

%% reconstruct odometry for visualization

pos = [0;0;0];
ori = [1;0;0;0];
%ori = right_pose.q(:,1);
t = [0];
traj_noisy = [t pos' ori'];

for iter_odom = 1:1:size(right_odom_n,1)
    t = right_odom_n(iter_odom,1);
    pos = pos + qRot(right_odom_n(iter_odom,2:4)', ori);
    ori = qProd( ori, v2q(right_odom_n(iter_odom,5:7)));
    traj_noisy = [traj_noisy; t pos' ori'];
end

%Ground Truth
groundTruth = [];
for iter_pose = 1:1:size(right_pose.p,2)
    t = right_pose.ts(1,iter_pose);
    pos = qRot(right_pose.p(:,iter_pose)-right_pose.p(:,1),q2qc(right_pose.q(:,1)));
    ori = qProd(q2qc(right_pose.q(:,1)), right_pose.q(:,iter_pose));
    groundTruth = [groundTruth; t pos' ori'];
end


%% visualization

% Plot trajectory
fig = figure();
subplot(2,2,1);
hold on;
set(fig, 'Name','reconstructed vs Ground Truth');
plot(traj_noisy(:,1),traj_noisy(:,2),'r');
plot(traj_noisy(1,1),traj_noisy(1,2),'og'); %start point
plot(traj_noisy(end,1),traj_noisy(end,2),'xg'); %end point
plot(right_pose.ts(:), groundTruth(:,2),'b');
legend('reconstructed','right_pose','end', 'ground_truth');
title('trajectory plot (X) wrt time')
grid on

subplot(2,2,2)
hold on;
plot(traj_noisy(:,1),traj_noisy(:,3),'r');
plot(traj_noisy(1,1),traj_noisy(1,3),'og'); %start point
plot(traj_noisy(end,1),traj_noisy(end,3),'xg'); %end point
plot(right_pose.ts(:), groundTruth(:,3),'b');
legend('reconstructed','start','end', 'ground_truth');
title('trajectory plot (Y) wrt time')
grid on

subplot(2,2,3)
hold on;
plot(traj_noisy(:,1),traj_noisy(:,4),'r');
plot(traj_noisy(1,1),traj_noisy(1,4),'og'); %start point
plot(traj_noisy(end,1),traj_noisy(end,4),'xg'); %end point
plot(right_pose.ts(:), groundTruth(:,4),'b');
legend('reconstructed','start','end', 'ground_truth');
title('trajectory plot (Z) wrt time')
grid on

subplot(2,2,4)
hold on;
plot3(traj_noisy(:,2),traj_noisy(:,3),traj_noisy(:,4),'r');
plot3(traj_noisy(1,2),traj_noisy(1,3),traj_noisy(1,4),'og'); %start point
plot3(traj_noisy(end,2),traj_noisy(end,3),traj_noisy(end,4),'xg'); %end point
plot3(groundTruth(:,2), groundTruth(:,3), groundTruth(:,4), 'b');
legend('reconstructed','start','end', 'ground_truth');
title('3D Ground truth')
grid on

fig = figure();
hold on;
set(fig, 'Name','Ground Truth');
plot(right_pose.ts(:), groundTruth(:,2),'r');
plot(right_pose.ts(:), groundTruth(:,3),'g');
plot(right_pose.ts(:), groundTruth(:,4),'b');
legend('X','Y','Z');
title('ground truth trajectory plot wrt time')
grid on

%% check orientation

% Plot orientation
fig = figure();
subplot(2,2,1);
hold on;
set(fig, 'Name','reconstructed vs Ground Truth');
plot(traj_noisy(:,1),traj_noisy(:,5),'r');
plot(traj_noisy(1,1),traj_noisy(1,5),'og'); %start point
plot(traj_noisy(end,1),traj_noisy(end,5),'xg'); %end point
plot(right_pose.ts(:), groundTruth(:,5),'b');
legend('reconstructed','start','end', 'ground_truth');
title('trajectory plot (QW) wrt time')
grid on

subplot(2,2,2)
hold on;
plot(traj_noisy(:,1),traj_noisy(:,6),'r');
plot(traj_noisy(1,1),traj_noisy(1,6),'og'); %start point
plot(traj_noisy(end,1),traj_noisy(end,6),'xg'); %end point
plot(right_pose.ts(:), groundTruth(:,6),'b');
legend('reconstructed','start','end', 'ground_truth');
title('trajectory plot (QX) wrt time')
grid on

subplot(2,2,3)
hold on;
plot(traj_noisy(:,1),traj_noisy(:,7),'r');
plot(traj_noisy(1,1),traj_noisy(1,7),'og'); %start point
plot(traj_noisy(end,1),traj_noisy(end,7),'xg'); %end point
plot(right_pose.ts(:), groundTruth(:,7),'b');
legend('reconstructed','start','end', 'ground_truth');
title('trajectory plot (QY) wrt time')
grid on

subplot(2,2,4)
hold on;
plot(traj_noisy(:,1),traj_noisy(:,8),'r');
plot(traj_noisy(1,1),traj_noisy(1,8),'og'); %start point
plot(traj_noisy(end,1),traj_noisy(end,8),'xg'); %end point
plot(right_pose.ts(:), groundTruth(:,8),'b');
legend('reconstructed','start','end', 'ground_truth');
title('trajectory plot (QZ) wrt time')
grid on

Eu_rec = [];
Eu_gr = [];
for iter = 1:1:size(traj_noisy,1)
    tmp = q2v(traj_noisy(iter, 5:8)');
    Eu_rec = [Eu_rec; tmp'];
    tmp = q2v(groundTruth(iter, 5:8)');
    Eu_gr = [Eu_gr; tmp'];
end

figure()
subplot(3,1,1);
hold on;
set(fig, 'Name','reconstructed vs Ground Truth');
plot(traj_noisy(:,1),Eu_rec(:,1),'r');
plot(right_pose.ts(:), Eu_gr(:,1),'b');
legend('reconstructed','ground_truth');
title('trajectory plot (OX) wrt time')
grid on

subplot(3,1,2)
hold on;
plot(traj_noisy(:,1),Eu_rec(:,2),'r');
plot(right_pose.ts(:), Eu_gr(:,2),'b');
legend('reconstructed','ground_truth');
title('trajectory plot (OY) wrt time')
grid on

subplot(3,1,3)
hold on;
plot(traj_noisy(:,1),Eu_rec(:,3),'r');
plot(right_pose.ts(:), Eu_gr(:,3),'b');
legend('reconstructed','ground_truth');
title('trajectory plot (OZ) wrt time')
grid on


% %% output data in txt file
% 
% %set orientation data in right order
% temp = traj_noisy;
% traj_noisy(:,5) = temp(:,8);
% traj_noisy(:,8) = temp(:,5);
% 
% filename_base = strsplit(bagfile,'.bag');
% filename_base = strjoin(filename_base(1));
% filename_base = strcat('/', filename_base);
% clear bagfile;
% 
% %reduce number of odometries before writting in file
% compress_odom = true;
% if(compress_odom)
%     compression_rate = 10;
%     right_odom1 = func_odom_compression(right_odom, compression_rate);
%     right_odom_n1 = func_odom_compression(right_odom_n, compression_rate);
%     KF_ts = [0.86 2.6 3.6 4.8 6.53 7.5 8.7 10.71];
%     right_odom_c = interKF_odom(right_odom, KF_ts);
%     right_odom_n = interKF_odom(right_odom_n, KF_ts);
% end
% 
% ground_truth_file = strcat(filename_base,'_ground_truth.txt');
% mpu_file = strcat(filename_base,'_mpu.txt');
% mpu_odom_file = strcat(filename_base,'_mpu_odom_extend.txt');
% mpu_odom_n_file = strcat(filename_base,'_mpu_odom_noise.txt');
% 
% save(strcat(pwd,ground_truth_file), 'groundTruth', '-ASCII');
% save(strcat(pwd,mpu_file), 'mpu6050_data', '-ASCII');
% save(strcat(pwd,mpu_odom_file), 'right_odom_c', '-ASCII');
% save(strcat(pwd,mpu_odom_n_file), 'right_odom', '-ASCII');